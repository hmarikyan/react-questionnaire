# Simple Questionnaire


## Steps to run app

### Must to have: Install node.js.

#### Run `npm install yarn -g`

#### Run `yarn install`

#### Run `yarn start`

#### Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



## Other scripts

#### `yarn test`

Launches the test runner in the interactive watch mode.\

#### `yarn build`

Builds the app for production to the `build` folder.\
