import React from 'react';

export const emptyAnswer = Symbol('empty answer');

export const QuestionsContext = React.createContext({});

export const QuestionsProvider = ({questions, children}) => {
    const [answeredQuestions, setAnswered] = React.useState({[questions[0].id]: emptyAnswer}); //{questionId: true/false}
    const [currentQuestionId, setCurrentQuestionId] = React.useState(questions[0].id);

    return <QuestionsContext.Provider
        value={{
            questions,

            answeredQuestions,
            setAnswered,

            currentQuestionId,
            setCurrentQuestionId,
        }}
    >
        {children}
    </QuestionsContext.Provider>
}
