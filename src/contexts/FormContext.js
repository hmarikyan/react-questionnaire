import React from 'react';
import useForceUpdate from "../hooks/useForceUpdate";
import Field from "../utils/Field";

export const FormContext = React.createContext({});

export const FormDataProvider = ({children}) => {
    const fields = React.useRef({});
    const forceUpdate = useForceUpdate();

    const values = Object
        .values(fields.current)
        .reduce((acc, field) => {
            acc[field.getName()] = field.getValue();
            return acc;
        }, {});

    const formErrorMessages = Object
        .values(fields.current)
        .reduce((acc, field) => {
            const errorMessages = field.getErrorMessages();
            if (errorMessages.length) {
                acc[field.getName()] = errorMessages;
            }
            return acc;
        }, {});


    const isFormValid = Object
        .values(formErrorMessages)
        .every(fieldErrorMessages => !fieldErrorMessages.length);

    const setFieldValue = React.useCallback(async (name, value, shouldValidate = true) => {
        const field = fields.current[name];

        await field.setValue(value, values, shouldValidate);
        forceUpdate();
    }, [forceUpdate, values]);

    const validateField = React.useCallback((name) => {
        const field = fields.current[name];
        const res = field.validate(values);

        forceUpdate();

        return res;
    }, [forceUpdate, values]);

    const validateForm = React.useCallback(async () => {
        const promises = Object
            .values(fields.current)
            .map(field => field.validate(values));

        const res = await Promise.all(promises);

        forceUpdate()
        return res.every(Boolean);
    }, [forceUpdate, values]);

    const resetForm = React.useCallback(() => {
        Object
            .values(fields.current)
            .forEach(field => field.reset());

        forceUpdate();
    }, [forceUpdate]);

    const resetField = React.useCallback((name) => {
        const field = fields.current[name];
        field.reset();

        forceUpdate();
    }, [forceUpdate]);

    const resetFieldValidationState = React.useCallback((name) => {
        const field = fields.current[name];
        field.resetValidationState();

        forceUpdate();
    }, [forceUpdate]);

    const registerField = React.useCallback((name, initialValue, validations, shouldValidate) => {
        fields.current[name] = new Field({
            initialValue: initialValue,
            validations: validations,
            name: name,
            shouldValidate: shouldValidate,
        });

        forceUpdate();
    }, [forceUpdate]);

    const formData = {
        registerField,
        fields: fields.current,

        values,
        setFieldValue,

        validateField,
        validateForm,
        isFormValid,
        formErrorMessages,

        resetForm,
        resetField,
        resetFieldValidationState,
    }

    return <FormContext.Provider value={formData}>
        {children}
    </FormContext.Provider>
}
