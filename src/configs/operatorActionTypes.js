const operatorActionTypes = {
    NOT_EQUAL: 'NOT_EQUAL',
    EQUAL: 'EQUAL',
}

export default operatorActionTypes;
