const validationTypes = {
    MIN_LENGTH: 'MIN_LENGTH',
    MAX_LENGTH: 'MAX_LENGTH',
    REQUIRED: 'REQUIRED',
    EQUALS: 'EQUALS',
    CUSTOM_FUNCTION: 'CUSTOM_FUNCTION'
}


export default validationTypes;
