const controlTypes = {
    TEXT: 'TEXT',
    NUMBER: 'NUMBER',
    RADIO: 'RADIO',
    DATE: 'DATE',
    CHECKBOX: 'CHECKBOX',
    SELECT: 'SELECT'
}

export default controlTypes;
