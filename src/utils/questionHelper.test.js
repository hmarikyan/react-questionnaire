import {cleanupAnsweredQuestions, getDisplayValue, getNextQuestion, isAnswered, isFieldEnabled} from "./questionHelper";
import {emptyAnswer} from "../contexts/QuestionsContext";
import operatorActionTypes from "../configs/operatorActionTypes";

describe('questionHelper tests', () => {
    describe('isAnswered tests', () => {
        it('should return true', () => {
            const value = '';
            expect(isAnswered(value)).toBe(true);

            const value2 = 'adwa';
            expect(isAnswered(value2)).toBe(true);

            const value3 = 0;
            expect(isAnswered(value3)).toBe(true);

            const value4 = null;
            expect(isAnswered(value4)).toBe(true);
        });

        it('should return false', () => {
            const value = undefined;
            expect(isAnswered(value)).toBe(false);

            const value2 = emptyAnswer;
            expect(isAnswered(value2)).toBe(false);
        });
    });

    describe('isFieldEnabled tests', () => {
        it('should consider boolean value', () => {
            const answeredQuestions = {QUESTION_1: 'answer 1'};
            const params = true;
            expect(isFieldEnabled(answeredQuestions, params)).toBe(true);

            const params2 = false;
            expect(isFieldEnabled(answeredQuestions, params2)).toBe(false);
        });

        it('should consider dependent question', () => {
            const answeredQuestions = {QUESTION_1: 'answer 1'};
            const params = {
                questionId: 'QUESTION_1',
                value: 'answer 1',
                condition: operatorActionTypes.EQUAL
            };
            expect(isFieldEnabled(answeredQuestions, params)).toBe(true);


            const answeredQuestions2 = {QUESTION_1: 'answer 1'};
            const params2 = {
                questionId: 'QUESTION_1',
                value: 'answer 1',
                condition: operatorActionTypes.NOT_EQUAL
            };
            expect(isFieldEnabled(answeredQuestions2, params2)).toBe(false);


            const answeredQuestions3 = {QUESTION_1: 'answer 1'};
            const params3 = {
                questionId: 'QUESTION_1',
                value: 'answer 1',
                condition: 'INVALID_CONDITION'
            };
            expect(isFieldEnabled(answeredQuestions3, params3)).toBe(true);

        })
    });

    describe('getNextQuestion tests', () => {
        it('should get the next question', () => {
            const questions = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: {
                        questionId: 'QUESTION_1',
                        value: 'answer 1',
                        condition: operatorActionTypes.EQUAL
                    },
                }
            ]
            const answeredQuestions = {
                QUESTION_1: 'answer 1',
            }
            expect(getNextQuestion(questions, answeredQuestions)).toBe(questions[1]);


            const questions2 = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: true,
                }
            ]
            const answeredQuestions2 = {
                QUESTION_1: 'answer 1',
                QUESTION_2: emptyAnswer,
            }
            expect(getNextQuestion(questions2, answeredQuestions2)).toBe(questions2[1]);


            const questions3 = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: true,
                }
            ]
            const answeredQuestions3 = {}
            expect(getNextQuestion(questions3, answeredQuestions3)).toBe(questions3[0]);
        });

        it('should NOT get the next question', () => {
            const questions = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: {
                        questionId: 'QUESTION_1',
                        value: 'answer 1',
                        condition: operatorActionTypes.NOT_EQUAL
                    },
                }
            ]
            const answeredQuestions = {
                QUESTION_1: 'answer 1',
            }
            expect(getNextQuestion(questions, answeredQuestions)).toBeFalsy();


            const questions2 = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: true,
                }
            ]
            const answeredQuestions2 = {
                QUESTION_1: 'answer 1',
                QUESTION_2: 'answer 2',
            }
            expect(getNextQuestion(questions2, answeredQuestions2)).toBeFalsy();

        });
    });


    describe('cleanupAnsweredQuestions tests', () => {
        it('should remove answered questions which are disabled', () => {
            const questions = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: false,
                }
            ]
            const answeredQuestions = {
                QUESTION_1: 'answer 1',
                QUESTION_2: 'answer 2'
            }

            expect(cleanupAnsweredQuestions(questions, answeredQuestions)).not.toHaveProperty('QUESTION_2');
        });

        it('should NOT remove any answered question', () => {
            const questions = [
                {
                    id: 'QUESTION_1',
                    enabled: true,
                },
                {
                    id: 'QUESTION_2',
                    enabled: true,
                }
            ]
            const answeredQuestions = {
                QUESTION_1: 'answer 1',
                QUESTION_2: 'answer 2'
            }

            expect(cleanupAnsweredQuestions(questions, answeredQuestions)).toBe(answeredQuestions);
        });
    });


    describe('getDisplayValue tests', () => {
        it('should return plain value', () => {
            const value = 'answer 1';

            expect(getDisplayValue(value, [])).toBe(value);
        });

        it('should return option label', () => {
            const value = 'OPTION_1';
            const options = [
                {
                    label: 'Option 1 label',
                    value: 'OPTION_1'
                },
                {
                    label: 'Option 2 label',
                    value: 'OPTION_2'
                }
            ]
            expect(getDisplayValue(value, options)).toBe(options[0].label);

            const value2 = 'OPTION_3';
            const options2 = [
                {
                    label: 'Option 1 label',
                    value: 'OPTION_1'
                },
                {
                    label: 'Option 2 label',
                    value: 'OPTION_2'
                }
            ]
            expect(getDisplayValue(value2, options2)).toBe(value2);
        });

        it('should return option label for multiple value', () => {
            const value = ['OPTION_1', 'OPTION_2'];
            const options = [
                {
                    label: 'Option 1 label',
                    value: 'OPTION_1'
                },
                {
                    label: 'Option 2 label',
                    value: 'OPTION_2'
                }
            ]
            expect(getDisplayValue(value, options)).toBe('Option 1 label, Option 2 label');

            const value2 = ['OPTION_1', 'OPTION_2', 'OPTION_3'];
            const options2 = [
                {
                    label: 'Option 1 label',
                    value: 'OPTION_1'
                },
                {
                    label: 'Option 2 label',
                    value: 'OPTION_2'
                }
            ]
            expect(getDisplayValue(value2, options2)).toBe('Option 1 label, Option 2 label, OPTION_3');
        })
    });
})

