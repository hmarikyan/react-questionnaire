import validationTypes from "../configs/validationTypes";
import {maxLengthValidator, minLengthValidator, requiredValidator} from "./validators";

class Field {
    #name;
    #value;
    #errorMessages;
    #shouldValidate;
    #isValid;
    #isPristine;

    constructor({name, isValid, isPristine, initialValue, validations, shouldValidate}) {
        this.#name = name;

        this.#isPristine = isPristine ?? true;

        this.#value = initialValue;
        this.initialValue = initialValue;

        this.#isValid = isValid ?? true;
        this.#shouldValidate = shouldValidate ?? true;
        this.validations = validations;
        this.#errorMessages = [];
    }

    getValue() {
        return this.#value;
    }

    getName() {
        return this.#name;
    }

    getErrorMessages() {
        return this.#errorMessages;
    }

    get isValid() {
        return this.#isValid;
    }

    get isPristine() {
        return this.#isPristine;
    }

    async validate(values) {
        if (!this.#shouldValidate) {
            return true;
        }

        this.#isValid = true;
        this.#errorMessages = [];

        this.validations.forEach(validation => {
            switch (validation.type) {
                case validationTypes.MAX_LENGTH: {
                    if (!maxLengthValidator(this.#value, {maxLength: validation.maxLength}, values)) {
                        this.#isValid = false;
                        this.#errorMessages.push(validation.message);
                    }
                    break;
                }
                case validationTypes.MIN_LENGTH: {
                    if (!minLengthValidator(this.#value, {minLength: validation.minLength}, values)) {
                        this.#errorMessages.push(validation.message);
                        this.#isValid = false;
                    }
                    break;
                }
                case validationTypes.REQUIRED: {
                    if (!requiredValidator(this.#value, {}, values)) {
                        this.#errorMessages.push(validation.message);
                        this.#isValid = false;
                    }
                    break;
                }
                case validationTypes.CUSTOM_FUNCTION: {
                    const errorMessage = validation.function(this.#value, this, values)
                    if (errorMessage !== true) {
                        this.#errorMessages.push(errorMessage || validation.message);
                        this.#isValid = false;
                    }
                    break;
                }
                default: {
                }
            }
        });

        return this.#isValid;
    }

    setShouldValidate(shouldValidate) {
        this.#shouldValidate = shouldValidate;
    }

    resetValidationState() {
        this.#isValid = true;
        this.#errorMessages = [];
    }

    async setValue(value, values, shouldValidate) {
        this.#value = value;
        this.#isPristine = false;

        if (this.#shouldValidate && shouldValidate) {
            await this.validate(values);
        }

        return true;
    }

    reset() {
        this.#isPristine = true;

        this.#errorMessages = [];
        this.#isValid = true;

        this.#value = this.initialValue;
    }
}

export default Field;
