import {maxLengthValidator, minLengthValidator, requiredValidator} from "./validators";

describe('validators tests', () => {
    it('tests minLength validator', () => {

        const value = '';
        expect(minLengthValidator(value, {minLength: 2})).toBe(false);

        const value2 = '123';
        expect(minLengthValidator(value2, {minLength: 2})).toBe(true);


        const value3 = 'sf';
        expect(minLengthValidator(value3, {minLength: 2})).toBe(true);
    });


    it('tests maxLength validator', () => {
        const value = 'sdfsf';
        expect(maxLengthValidator(value, {maxLength: 2})).toBe(false);

        const value2 = '12';
        expect(maxLengthValidator(value2, {maxLength: 2})).toBe(true);


        const value3 = '';
        expect(maxLengthValidator(value3, {maxLength: 2})).toBe(true);
    });

    it('tests required validator', () => {
        const value = 'sdfsf';
        expect(requiredValidator(value)).toBe(true);

        const value2 = 0;
        expect(requiredValidator(value2)).toBe(true);

        const value3 = '';
        expect(requiredValidator(value3)).toBe(false);


        const value4 = null;
        expect(requiredValidator(value4)).toBe(false);
    })

});
