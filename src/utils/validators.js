export const minLengthValidator = (value, {minLength}, values) => {
    return value.trim().length >= minLength;
}

export const maxLengthValidator = (value, {maxLength}, values) => {
    return value.trim().length <= maxLength;
}

export const requiredValidator = (value, params, values) => {
    if(Array.isArray(value)) {
        return value.length;
    }
    return value !== null && value !== undefined && value !== '';
}
