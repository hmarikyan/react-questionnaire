import operatorActionTypes from "../configs/operatorActionTypes";
import {emptyAnswer} from "../contexts/QuestionsContext";

export const isAnswered = (value) => {
    return ![undefined, emptyAnswer].includes(value);
}

export const isFieldEnabled = (answeredQuestions, params) => {
    if (typeof params === 'boolean') {
        return params;
    }

    const currentValue = answeredQuestions[params.questionId];

    switch (params.condition) {
        case operatorActionTypes.EQUAL: {
            return params.value === currentValue;
        }
        case operatorActionTypes.NOT_EQUAL: {
            return params.value !== currentValue;
        }
        default : {
            return true;
        }
    }
}

export const getNextQuestion = (questions, answeredQuestions) => {
    return questions.find(q => !isAnswered(answeredQuestions[q.id]) && isFieldEnabled(answeredQuestions, q.enabled));
}

export const cleanupAnsweredQuestions = (questions, answeredQuestions) => {
    questions.forEach(q => {
        if (isAnswered(answeredQuestions[q.id]) && !isFieldEnabled(answeredQuestions, q.enabled)) {
            delete answeredQuestions[q.id];
        }
    })

    return answeredQuestions;
}


export const getDisplayValue = (value, options) => {
    if (options && options.length) {
        if (Array.isArray(value)) {
            return value
                .map(v => options.find(o => o.value === v)?.label ?? v)
                .join(', ');
        } else {
            return options.find(o => o.value === value)?.label ?? value;
        }
    } else {
        return value;
    }
}


