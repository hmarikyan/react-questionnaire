import React from "react";

const useForceUpdate = () => {
    const set = React.useState(0)[1];
    return () => set((s) => s + 1);
};

export default useForceUpdate;
