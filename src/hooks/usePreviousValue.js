import React from "react";

const usePreviousValue = (value) => {
    const ref = React.useRef();

    React.useEffect(() => {
        ref.current = value;
    }, [value]); // re-run if value changes

    return ref.current;
}

export default usePreviousValue;
