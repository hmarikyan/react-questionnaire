import React from "react";
import {emptyAnswer, QuestionsContext} from "../contexts/QuestionsContext";
import {isFieldEnabled, cleanupAnsweredQuestions, getNextQuestion, isAnswered} from "../utils/questionHelper";

const useQuestions = () => {
    const {
        questions,

        answeredQuestions,
        setAnswered,

        currentQuestionId,
        setCurrentQuestionId,
    } = React.useContext(QuestionsContext);

    const answerQuestion = React.useCallback((question, value) => {
        let newAnsweredQuestions = {...answeredQuestions};
        newAnsweredQuestions[question.id] = value;

        newAnsweredQuestions = cleanupAnsweredQuestions(questions, newAnsweredQuestions);

        const nextQuestion = getNextQuestion(questions, newAnsweredQuestions);

        setCurrentQuestionId(nextQuestion ? nextQuestion.id : null);
        if (nextQuestion) {
            newAnsweredQuestions[nextQuestion.id] = emptyAnswer;
        }

        setAnswered(newAnsweredQuestions);
    }, [questions, answeredQuestions, setAnswered, setCurrentQuestionId]);

    const isAllAnswered = React.useMemo(() => {
        return Object.values(answeredQuestions).filter(isAnswered).length
            >=
            questions.filter(q => isFieldEnabled(answeredQuestions, q.enabled)).length;
    }, [questions, answeredQuestions]);

    return {
        questions,

        answeredQuestions,
        answerQuestion,
        isAllAnswered,

        currentQuestionId,
        setCurrentQuestionId,
    };
}

export default useQuestions;
