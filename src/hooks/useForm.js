import React from "react";
import usePreviousValue from "./usePreviousValue";
import {FormContext} from "../contexts/FormContext";


export const useForm = () => {
    return React.useContext(FormContext);
}

export const useField = ({name, initialValue, validations, shouldValidate}) => {
    const formObject = useForm();
    const field = formObject.fields[name];

    const prevShouldValidate = usePreviousValue(shouldValidate);

    const value = formObject.values[name];
    const errorMessages = field?.getErrorMessages() || [];
    const isPristine = field?.isPristine ?? true;
    const isValid = field?.isValid ?? true;

    React.useEffect(() => {
        formObject.registerField(name, initialValue, validations, shouldValidate);
        // eslint-disable-next-line
    }, [name]);

    React.useEffect(() => {
        if (field && prevShouldValidate !== shouldValidate) {
            field.setShouldValidate(shouldValidate, formObject.values);
            if (shouldValidate) {
                formObject.validateField(name);
            } else {
                formObject.resetFieldValidationState(name);
            }
        }
    }, [formObject, field, shouldValidate, name, prevShouldValidate]);


    const setValue = async function (value, shouldValidate) {
        return formObject.setFieldValue(name, value, shouldValidate);
    }

    const validate = async function () {
        return await formObject.validateField(name);
    }

    const reset = function () {
        return formObject.resetField(name);
    }

    return {
        field,

        value,
        isPristine,
        isValid,

        validate,
        errorMessages,

        setValue,

        reset,
    }
}
