import { render } from '@testing-library/react';
import App from './App';

test('renders QuestionsContainer', () => {
  const renderResult = render(<App />);
  const questionContainer = renderResult.getByTestId('QuestionsContainer');
  expect(questionContainer).toBeInTheDocument();
});
