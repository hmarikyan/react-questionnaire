import './App.scss';
import QuestionsContainer from "./Questions/QuestionsContainer";
import {QuestionsProvider} from "../contexts/QuestionsContext";
import data from "../configs/data.json";
import {FormDataProvider} from "../contexts/FormContext";

function App() {
    return (
        <div className="App">
            <QuestionsProvider questions={data}>
                <FormDataProvider>
                    <QuestionsContainer/>
                </FormDataProvider>
            </QuestionsProvider>
        </div>
    );
}

export default App;
