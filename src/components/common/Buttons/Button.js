import PropTypes from "prop-types";
import "./Buttons.scss";

const Button = ({children, onClick, className, disabled, type}) => {
    const classNames = ['Button', className, type];

    return <button
        className={classNames.join(' ').trim()}
        onClick={onClick}
        disabled={disabled}
    >
        {children}
    </button>
}

Button.propTypes = {
    type: PropTypes.oneOf([
        'primary',
        'link',
    ]),
    onClick: PropTypes.func,
    className: PropTypes.string,
    disabled: PropTypes.bool,
}

Button.defaultProps = {
    className: '',
    disabled: false,
    type: 'primary',
}

export default Button;
