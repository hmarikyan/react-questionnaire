import PropTypes from "prop-types";
import './Number.scss';

const Number = ({className, value, onChange, disabled}) => {
    const classNames = ['Number', className];

    return <input
        disabled={disabled}
        className={classNames.join(' ').trim()}
        type="number"
        value={value}
        onChange={onChange}
    />
}

Number.propTypes = {
    disabled: PropTypes.bool,
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired
}

Number.defaultProps = {
    className: '',
    value: '',
    disabled: false,
}

export default Number;
