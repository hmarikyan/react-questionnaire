import PropTypes from "prop-types";
import './Date.scss';

const Date = ({className, value, onChange, disabled}) => {
    const classNames = ['Date', className];

    return <input
        disabled={disabled}
        className={classNames.join(' ').trim()}
        type="date"
        value={value}
        onChange={onChange}
    />
}

Date.propTypes = {
    disabled: PropTypes.bool,
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired
}

Date.defaultProps = {
    className: '',
    value: '',
    disabled: false,
}

export default Date;
