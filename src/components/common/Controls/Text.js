import PropTypes from "prop-types";
import "./Text.scss";

const Text = ({className, value, onChange, disabled}) => {
    const classNames = ['Text', className];

    return <input
        disabled={disabled}
        className={classNames.join(' ').trim()}
        type="text"
        value={value}
        onChange={onChange}
    />
}

Text.propTypes = {
    disabled: PropTypes.bool,
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired
}

Text.defaultProps = {
    className: '',
    value: '',
    disabled: false,
}

export default Text;
