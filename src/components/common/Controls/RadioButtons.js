import PropTypes from "prop-types";
import './RadioButtons.scss';

const RadioButtons = ({className, value, onChange, options, disabled}) => {
    const classNames = ['RadioButtons', className];

    return <div
        className={classNames.join(' ').trim()}
        onChange={onChange}
    >
        {options.map((option) => {
            return <label key={option.value} className={'RadioButtons__label'}>
                <input
                    className={'RadioButtons__input'}
                    disabled={disabled}
                    type="radio"
                    value={option.value}
                    checked={option.value === value}
                    name={option.label}
                    onChange={() => null}
                />

                {option.label}
            </label>
        })}
    </div>
}

RadioButtons.propTypes = {
    disabled: PropTypes.bool,
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.any,
            label: PropTypes.string
        })
    ),
}

RadioButtons.defaultProps = {
    className: '',
    value: '',
    options: [],
    disabled: false
}


export default RadioButtons
