import PropTypes from "prop-types";
import './Select.scss';

const Select = ({className, value, onChange, options, disabled}) => {
    const classNames = ['Select', className];

    return <select
        value={value}
        onChange={onChange}
        disabled={disabled}
        className={classNames.join(' ').trim()}
    >
        <option className={'Select__option'} value="" disabled>Select one--</option>
        {options.map(({value, label}) => {
            return <option className={'Select__option'} key={value} value={value}>{label}</option>
        })}
    </select>
}

Select.propTypes = {
    disabled: PropTypes.bool,
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.any,
            label: PropTypes.string
        })
    ),
}

Select.defaultProps = {
    className: '',
    value: '',
    options: [],
    disabled: false,
}


export default Select;
