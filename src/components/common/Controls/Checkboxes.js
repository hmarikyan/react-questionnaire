import PropTypes from "prop-types";
import './Checkboxes.scss';

const Checkboxes = ({className, value: valueArray, onChange, options, disabled}) => {
    const classNames = ['Checkboxes', className];

    const onInternalChange = (e) => {
        const v = e.target.value;

        let newValue = [...valueArray];
        if (valueArray.includes(v)) {
            newValue = newValue.filter(option => option !== v);
        } else {
            newValue.push(v);
        }

        onChange({
            target: {
                value: newValue.filter(Boolean)
            }
        });
    }

    return <div
        className={classNames.join(' ').trim()}
        onChange={onInternalChange}
    >
        {options.map((option) => {
            return <label className={'Checkboxes__label'} key={option.value}>
                <input
                    className={'Checkboxes__input'}
                    disabled={disabled}
                    type="checkbox"
                    value={option.value}
                    checked={valueArray.includes(option.value)}
                    name={option.label}
                    onChange={() => null}
                />
                {option.label}
            </label>
        })}
    </div>
}

Checkboxes.propTypes = {
    disabled: PropTypes.bool,
    className: PropTypes.string,
    value: PropTypes.arrayOf(PropTypes.any),
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.any,
            label: PropTypes.string
        })
    ),
}

Checkboxes.defaultProps = {
    className: '',
    value: [],
    options: [],
    disabled: false,
}


export default Checkboxes
