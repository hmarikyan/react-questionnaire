import PropTypes from "prop-types";
import './ErrorMessages.scss';

const ErrorMessages = ({messages}) => {
    return messages && messages.length ? <div className={'ErrorMessages'}>
        {messages.map((message, key) => <p className={'ErrorMessages__ErrorMessage'} key={key}>{message}</p>)}
    </div> : null;
}

ErrorMessages.propTypes = {
    messages: PropTypes.arrayOf(
        PropTypes.string
    ),
}

export default ErrorMessages;
