import PropTypes from "prop-types";
import "./StatusCircle.scss";

const StatusCircle = ({status}) => {
    const classNames = ['StatusCircle', status];

    return <div className={classNames.join(' ')}/>
}

StatusCircle.propTypes = {
    status: PropTypes.oneOf(['complete', 'partially-complete'])
}

export default StatusCircle;
