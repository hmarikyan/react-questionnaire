import controlTypes from "../../configs/controlTypes";
import PropTypes from "prop-types";

export const optionPropType = PropTypes.shape({
    value: PropTypes.any,
    label: PropTypes.string
});

export const questionPropType = PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    type: PropTypes.oneOf(Object.keys(controlTypes)).isRequired,
    value: PropTypes.any,
    enabled: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.shape({
            condition: PropTypes.string,
            questionId: PropTypes.string,
            value: PropTypes.any,
        })
    ]),
    options: PropTypes.arrayOf(
        optionPropType
    ),
    validations: PropTypes.arrayOf(
        PropTypes.shape({
            type: PropTypes.string,
            message: PropTypes.string,
        })
    ),
}).isRequired;

export const fieldObjectPropType = PropTypes.shape({
    value: PropTypes.any,
    setValue: PropTypes.func,
    errorMessages: PropTypes.arrayOf(PropTypes.string),
    isValid: PropTypes.bool,
    reset: PropTypes.func,
    validate: PropTypes.func,
    isPristine: PropTypes.bool,
})
