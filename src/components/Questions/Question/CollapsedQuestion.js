import React from "react";

import './CollapsedQuestion.scss';

import useQuestions from "../../../hooks/useQuestions";
import StatusCircle from "../../common/StatusCircle/StatusCircle";
import {getDisplayValue, isAnswered} from "../../../utils/questionHelper";
import {questionPropType} from "../propTypes";


const CollapsedQuestion = ({question}) => {
    const {isAllAnswered, setCurrentQuestionId, answeredQuestions} = useQuestions();

    let displayValue = React.useMemo(
        () => getDisplayValue(answeredQuestions[question.id], question.options),
        [answeredQuestions, question]
    );

    const expandQuestion = React.useCallback(() => {
        setCurrentQuestionId(question.id);
    }, [setCurrentQuestionId, question])

    return <div className={'CollapsedQuestion'}>
        <div
            className={'CollapsedQuestion__title'}
            onClick={expandQuestion}
        >
            {question.title}
        </div>
        <div className={'CollapsedQuestion__answer'}>
            {displayValue}
        </div>
        <div className={'CollapsedQuestion__status'}>
            {isAnswered(answeredQuestions[question.id]) ?
                <StatusCircle status={isAllAnswered ? 'complete' : 'partially-complete'}/> : null}
        </div>
    </div>;
}

CollapsedQuestion.propTypes = {
    question: questionPropType,
}

export default CollapsedQuestion;
