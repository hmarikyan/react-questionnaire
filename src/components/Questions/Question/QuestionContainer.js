import React from "react";
import PropTypes from "prop-types";

import './QuestionContainer.scss';

import controlTypes from "../../../configs/controlTypes";
import CurrentQuestion from "./CurrentQuestion";
import CollapsedQuestion from "./CollapsedQuestion";
import {useField, useForm} from "../../../hooks/useForm";
import {questionPropType} from "../propTypes";


const QuestionContainer = ({question, isCurrent}) => {
    const formObject = useForm();

    const fieldObject = useField({
        name: question.id,
        initialValue: question.type === controlTypes.CHECKBOX ? [question.value].filter(Boolean) : question.value,
        validations: question.validations,
    })

    let content;
    if (isCurrent) {
        content = <CurrentQuestion
            question={question}
            fieldObject={fieldObject}
            formObject={formObject}
        />;
    } else {
        content = <CollapsedQuestion
            question={question}
        />;
    }

    return <div className={'QuestionContainer'} data-testid={'QuestionContainer_'+question.id}>
        {content}
    </div>;
}

QuestionContainer.propTypes = {
    question: questionPropType,
    isCurrent: PropTypes.bool,
}


export default QuestionContainer;
