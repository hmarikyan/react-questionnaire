import React from "react";
import './CurrentQuestion.scss';

import controlTypes from "../../../configs/controlTypes";
import Text from "../../common/Controls/Text";
import Select from "../../common/Controls/Select";
import RadioButtons from "../../common/Controls/RadioButtons";
import Date from "../../common/Controls/Date";
import Checkboxes from "../../common/Controls/Checkboxes";
import Number from "../../common/Controls/Number";
import ErrorMessages from "../../common/ErrorMessages/ErrorMessages";
import useQuestions from "../../../hooks/useQuestions";
import Button from "../../common/Buttons/Button";
import {isAnswered} from "../../../utils/questionHelper";
import {fieldObjectPropType, questionPropType} from "../propTypes";


const CurrentQuestion = ({question, fieldObject}) => {
    const {answerQuestion, answeredQuestions} = useQuestions();

    const {value, setValue, errorMessages, isValid, reset} = fieldObject;

    const onChange = React.useCallback(({target}) => {
        setValue(target.value);
    }, [setValue]);

    const onSubmit = React.useCallback(async () => {
        if (await fieldObject.validate()) {
            answerQuestion(question, value);
        }
    }, [fieldObject, question, answerQuestion, value]);

    const onCancel = React.useCallback(() => {
        reset();
        setValue(answeredQuestions[question.id]);
        answerQuestion(question, answeredQuestions[question.id]);
    }, [reset, setValue, answerQuestion, question, answeredQuestions])

    const control = React.useMemo(() => {
        switch (question.type) {
            case controlTypes.TEXT: {
                return <Text
                    value={value}
                    onChange={onChange}
                />
            }
            case controlTypes.SELECT: {
                return <Select
                    value={value}
                    onChange={onChange}
                    options={question.options}
                />
            }
            case controlTypes.RADIO: {
                return <RadioButtons
                    value={value}
                    onChange={onChange}
                    options={question.options}
                />
            }
            case controlTypes.DATE: {
                return <Date
                    value={value}
                    onChange={onChange}
                />
            }
            case controlTypes.CHECKBOX: {
                return <Checkboxes
                    value={value}
                    onChange={onChange}
                    options={question.options}
                />
            }
            case controlTypes.NUMBER: {
                return <Number
                    value={value}
                    onChange={onChange}
                />
            }

            default: {
                return <Text
                    value={value}
                    onChange={onChange}
                />
            }

        }
    }, [question, onChange, value]);

    return <div className={'CurrentQuestion'}>
        <div className={'CurrentQuestion__TitleWrapper'}>
            <h3 className={'CurrentQuestion__Title'}>{question.title}</h3>
        </div>

        {question.description ? <div className={'CurrentQuestion__DescriptionWrapper'}>
            <p className={'CurrentQuestion__Description'}>{question.description}</p>
        </div> : null}

        <div className={'CurrentQuestion__ControlWrapper'}>
            {control}
        </div>

        <div className={'CurrentQuestion__ErrorMessagesWrapper'}>
            <ErrorMessages messages={errorMessages}/>
        </div>

        <div className={'CurrentQuestion__ActionsWrapper'}>
            <div>
                <Button
                    disabled={!isValid}
                    onClick={onSubmit}
                    type={'primary'}
                >
                    Save
                </Button>
            </div>
            {isAnswered(answeredQuestions[question.id]) && <div>
                <Button
                    onClick={onCancel}
                    type={'link'}
                >
                    Cancel
                </Button>
            </div>}

        </div>
    </div>;
}

CurrentQuestion.propTypes = {
    question: questionPropType,
    fieldObject: fieldObjectPropType,
}

export default CurrentQuestion
