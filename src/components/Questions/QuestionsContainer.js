import useQuestions from "../../hooks/useQuestions";
import QuestionContainer from "./Question/QuestionContainer";
import './QuestionsContainer.scss';

const QuestionsContainer = () => {
    const {questions, answeredQuestions, currentQuestionId, isAllAnswered} = useQuestions();

    return <div className={'QuestionsContainer'} data-testid={'QuestionsContainer'}>
        <div className={'QuestionsContainer__title'}>
            <h2>Please answer following questions</h2>
        </div>
        <div className={'QuestionsContainer__questions'}>
            {questions
                .filter(q => (answeredQuestions[q.id] !== undefined || q.id === currentQuestionId))
                .map(question => {
                    return <QuestionContainer
                        isCurrent={question.id === currentQuestionId}
                        key={question.id}
                        question={question}
                    />
                })}
        </div>

        {isAllAnswered ? <div className={'QuestionsContainer__done'}>
            <h1>Thank you !!</h1>
        </div>: null}
    </div>
}


export default QuestionsContainer;
